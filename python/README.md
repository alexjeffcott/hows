Getting Going with Python
=========================
Step 1: Create a git repo and push it to Bitbucket
-------------------------------------------------
see the git README.md for getting set up the repo

Step 2: add virtual environment
-------------------------------
1. Open an integrated terminal window in the project folder and run:
>python3 -m venv .venv
>source .venv/bin/activate
2. Add a .gitignore with the following lines:
>.idea
>.venv
>.DS_Store
3. Add a readme markdown file to keep track of what is happening ;) run:
>touch readme.md
4. create a 'packages file' by running:
>pip3 freeze > requirements.txt
5. Commit progress and push to remote as 'initial setup'.


PANDA
____________________________________________________
###Assumes:
    *macOs
    *python3 is installed
    *virtualenv is installed
    *XCode is installed
    *Android Studio is installed

##Steps
1. Create remote repo in bitbucket via sourcetree and clone and add to vscode workspace.
2. Open an integrated terminal window in the project folder and run:
>python3 -m venv .venv
>source .venv/bin/activate
3. Add a .gitignore with the following lines:
>touch .gitignore
>.idea
>.venv
>.DS_Store
4. Add a readme markdown file to keep track of what is happening ;) run:
>touch readme.md
[add the how tos below]
5. 
>pip freeze > requirements.txt
6. Commit progress and push to remote as 'initial setup'.



mock data source url:
url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/car/car.data'

"""
##How Tos
###To activate the virtual environment run:
>source .venv/bin/activate

###To deactivate the virtual environment run:
>deactivate

###To create ‘package.json’ run:
>pip freeze > requirements.txt

###To ‘npm install’ run:
>pip install -r requirements.txt

####Here is the vscode config:
{
    "window.zoomLevel": -1,
    "editor.rulers": [
        120
    ],
    "editor.minimap.enabled": false,
    "editor.minimap.renderCharacters": false,
    "editor.formatOnSave": true,
    "python.pythonPath": ".venv/bin/python",
    "python.autoComplete.addBrackets": true,
    "python.linting.pep8Enabled": true,
    "python.linting.mypyEnabled": false,
    "python.linting.pep8Args": [
        "--max-line-length=120",
        "--extension-pkg-whitelist=cv2",
    ],
    "python.linting.pylintArgs": [
        "--extension-pkg-whitelist=cv2"
    ],
    "python.linting.enabled": true,
    "python.linting.pylintUseMinimalCheckers": false,
    "python.linting.pylintEnabled": false
}
"""