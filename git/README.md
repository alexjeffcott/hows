Getting Going with Git
======================
Step1: Create a git repo and push it to Bitbucket
-------------------------------------------------
1. Go to bitbucket.org and sign in
2. Click on the ‘Create’ button then subsequently ‘repository’ on the left
3. Choose a repository name (something sensible which describes your project - no spaces are allowed and you should avoid using numbers or anything too long).
4. Click ‘Create repository’.
5. Copy the `git clone [your new repo’s url]` it from under where it says ‘Let’s put some its in your bucket’.
6. Open a terminal window in the folder where you want to keep the project folder.
7. Paste the command you just copied and hit enter - it will warn you that you have cloned an empty repo but that is fine (and accurate).
8. Close the Terminal window and open PyCharm.
9. Click ‘open’ and choose the folder you just created (the one in the folder where you keep your projects which has the name of the repo you just created and downloaded to your computer). Take care not check 'add to existing projects' when it asks you whether you want to open in the current or a new window.
10. Right click — or ctrl click ;) — your project folder on the left, go to ‘new’ and choose ‘file’ which you should name ‘README.md’ then click ‘ok’.
If it asks you whether you want to add the file to git, you should click ‘ok’.
The file should open automatically, you should type ‘Hello World!’ and close the file’s tab.
Now open Terminal from within PyCharm and enter the following commands:
```
    git add .
    git commit --m "first commit"
    git push origin master
```
NB:
- Don’t forget the space fullstops after `add`
- `--m` is ‘dash dash m’
- You need to use double quotes

You can confirm whether this has worked by returning to the bitbucket page where you copied the clone command, and hitting refresh: you should see a README.md file.

If you get stopped at any point where it asks you for a PW or sth like that, then you likely need to set up SSH. This is easy, but you should halt the process and follow the steps below under 'Setting up SSH in bitbucket'.

Setting up SSH in bitbucket
---------------------------
1. Go to bitbucket.org
2. Click on your profile icon (a ‘blank’ avatar by default) in the bottom left.
3. Choose ‘bitbucket settings’ then, under ‘SECURITY’ choose ‘SSH keys’.
4. Open a new Terminal window and enter `ssh-keygen`.
5. Hit enter to choose the default location and leave the passphrase blank to not create one.
6. Enter `cat ~/.ssh/id_rsa.pub | pbcopy` to copy the key.
7. Return to the bitbucket profile/settings/security/SSH keys page and click the ‘Add key’ button.
8. In the ‘Key*’ field paste your key.
9. Add a meaningful label in the ‘Label’ field.
10. Click the ‘Add key’ button.