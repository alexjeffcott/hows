Getting Going with vsCode
=========================
Step 1: Create a git repo and push it to Bitbucket
-------------------------------------------------
install vscode
install the following plugins:
gitblame
debugger for chrome
eslint
docker
markdown preview enhansed
markdownlint
python
YAML

go to settings and toggle the json option.
add the following to the usersettings config
####Here is the vscode config:
{
    "editor.rulers": [
        120
    ],
    "editor.minimap.enabled": false,
    "editor.minimap.renderCharacters": false,
    "editor.formatOnSave": true,
    "python.pythonPath": ".venv/bin/python",
    "python.autoComplete.addBrackets": true,
    "python.linting.pep8Enabled": true,
    "python.linting.mypyEnabled": false,
    "python.linting.pep8Args": [
        "--max-line-length=120",
        "--extension-pkg-whitelist=cv2",
    ],
    "python.linting.pylintArgs": [
        "--extension-pkg-whitelist=cv2"
    ],
    "python.linting.enabled": true,
    "python.linting.pylintUseMinimalCheckers": false,
    "python.linting.pylintEnabled": false
}





{
    "editor.tabSize": 2,
    "editor.wordWrap": "wordWrapColumn",
    "editor.wordWrapColumn": 120,
    "files.exclude": {
        "**/.git": true,
        "**/.svn": true,
        "**/.hg": true,
        "**/CVS": true,
        "**/.DS_Store": true,
        "**/.venv": true
    },
    "gitblame.ignoreWhitespace": true,
    "python.linting.pep8Enabled": true,
    "emmet.excludeLanguages": [
        "markdown",
        "javascript",
        "javascriptreact"
    ],
    "eslint.autoFixOnSave": true,
    "eslint.run": "onSave"
}